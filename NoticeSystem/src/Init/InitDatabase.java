package Init;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InitDatabase extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public InitDatabase() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	
	
	
	public void init() throws ServletException {
		// Put your code here
		System.out.println("NoticeSystem-log：数据库连接信息设定中……");
		ServletContext application=getServletContext();
		String dburl=getInitParameter("dburl");
		String username=getInitParameter("username");
		String password=getInitParameter("password");
		String smtphost=getInitParameter("smtphost");
		String smtpusername=getInitParameter("smtpusername");
		String smtppassword=getInitParameter("smtppassword");
		String thishost=getInitParameter("thishost");
		String privatehost=getInitParameter("privatehost");
		application.setAttribute("dburl", dburl+"?useUnicode=true&characterEncoding=utf8");
		application.setAttribute("username", username);
		application.setAttribute("password", password);
		application.setAttribute("smtphost", smtphost);
		application.setAttribute("smtpusername", smtpusername);
		application.setAttribute("smtppassword", smtppassword);
		application.setAttribute("thishost", thishost);
		application.setAttribute("privatehost", privatehost);
		System.out.println("NoticeSystem-log：application-datastick-database_url="+dburl+";");
		System.out.println("NoticeSystem-log：application-datastick-database_username="+username+";");
		System.out.println("NoticeSystem-log：application-datastick-database_password="+password+";");
		System.out.println("NoticeSystem-log：application-datastick-smtp_host="+smtphost+";");
		System.out.println("NoticeSystem-log：application-datastick-smtp_username="+smtpusername+";");
		System.out.println("NoticeSystem-log：application-datastick-smtp_password="+smtppassword+";");
		System.out.println("NoticeSystem-log：application-datastick-thishost="+thishost+";");
		System.out.println("NoticeSystem-log：application-datastick-privatehost="+privatehost+";");
		System.out.println("NoticeSystem-log：数据库连接信息设定完毕！");
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		init();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		init();
	}

}
