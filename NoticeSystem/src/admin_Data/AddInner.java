package admin_Data;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Email.SendMail;

public class AddInner extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public AddInner() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>Servlet�ڲ�ʹ��</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("这个Servlet并不是公开的，请不要直接访问。");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	ServletContext application;
	
	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		application=getServletContext();
		String caption=request.getParameter("caption");
		String summary=request.getParameter("summary");
		String detail=request.getParameter("detail");
		String username=request.getParameter("username");
		String id=request.getParameter("id");
		String sqls1="INSERT INTO notice(username,caption,summary,detail) VALUE('"+username+"','"+caption+"','"+summary+"','"+detail+"')";
		int result=-1;
		ResultSet rs=null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(application.getAttribute("dburl").toString()+"", application.getAttribute("username").toString(), application.getAttribute("password").toString());
			Statement sm=con.createStatement();
			result=sm.executeUpdate(sqls1);
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			out.println(e);
			out.flush();
			out.close();
			return ;
		}
		int len=0;
		if(result==-1){
			System.err.println("NoticeSystem-log：PublicEdit-无法更改信息或插入新信息！");
		}else {
			if(len!=0){
				try {
					rs.next();
				} catch (SQLException e1) {
					// TODO 自动生成的 catch 块
					e1.printStackTrace();
				}
				for(int i=0;i<len;++i){
					try {
						SendEmail(caption, username, summary, detail,rs.getString(2),rs.getString(1));
						rs.next();
					} catch (Exception e) {
						// TODO 自动生成的 catch 块
						e.printStackTrace();
					}
				}
			}
			out.print("true");
			out.flush();
			out.close();
		}
	}

	private void SendEmail(String caption,String username,String summary,String detail,String sendto,String sendid) throws Exception{
		String thishost=(String) application.getAttribute("thishost");
		String smtphost=(String) application.getAttribute("smtphost");
		String smtpusername=(String) application.getAttribute("smtpusername");
		String smtppassword=(String) application.getAttribute("smtppassword");
		String content="<center><h1><font color=\"red\">Jstudio-NoticeSystem公告信息</font></h1>"
				+ "<table>"
				+ "<tr>"
				+ "<td>公告类别：</td>"
				+ "<td>内部公告</td>"
				+ "</tr>"
				+ "<tr>"
				+ "<td>标题：</td>"
				+ "<td>"+caption+"</td>"
				+ "</tr>"
				+ "<tr>"
				+ "<td>发布者：</td>"
				+ "<td>"+username+"</td>"
				+ "</tr>"
				+ "<tr>"
				+ "<td>简介：</td>"
				+ "<td>"+summary+"</td>"
				+ "</tr>"
				+ "<tr>"
				+ "<td>详细内容：</td>"
				+ "<td>"+detail+"</td>"
				+ "</tr>"
				+ "<tr>"
				+ "<td>相关操作：</td>"
				+ "<td>"
				+ "<a href=\""+thishost+"/inner-notice.jsp\">访问内部公告</a><br/>"
				+ "<a href=\""+thishost+"/inner/unsubscribe.jsp?id="+sendid+"\">退订邮件推送</a><br/>"
				+ "</td>"
				+ "</tr>"
				+ "</table>"
				+ "</center>";
		System.out.println(content);
		SendMail.send(smtphost, smtpusername, smtppassword, smtpusername, sendto, "NoticeSystem推送："+caption, content);
	}
	
	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
