package Email;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.mail.imap.protocol.ID;

public class InnerEmail extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public InnerEmail() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>Servlet�ڲ�ʹ��</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("这个Servlet并不是公开的，请不要直接访问。");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		String username=null;
		Cookie[] cookie=request.getCookies();
		if(cookie!=null){
			for(int i=0;i<cookie.length;i++){
				if(cookie[i].getName().equals("NSUN")==true){
					username=cookie[i].getValue();
				}
			}
		}
		ServletContext application=getServletContext();
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		String email=request.getParameter("email");
		String sqls1="INSERT INTO iemail(email,username) VALUE('"+email+"','"+username+"')";
		int result=-1;
		ResultSet rs=null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(application.getAttribute("dburl").toString()+"", application.getAttribute("username").toString(), application.getAttribute("password").toString());
			Statement sm=con.createStatement();
			result=sm.executeUpdate(sqls1);
		} catch (Exception e) {
			// TODO 自动生成的 catch 块
			out.println(e);
			out.flush();
			out.close();
			return ;
		}
		if(result==-1){
			System.err.println("NoticeSystem-log：PublicEdit-无法更改信息或插入新信息！");
			out.print("false");
			return ;
		}
		out.print("true");
		out.flush();
		out.close();
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
