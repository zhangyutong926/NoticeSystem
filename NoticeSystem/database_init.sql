/*
 Navicat Premium Data Transfer

 Source Server         : Localhost Mysql Database
 Source Server Type    : MySQL
 Source Server Version : 50617
 Source Host           : localhost
 Source Database       : noticesystem

 Target Server Type    : MySQL
 Target Server Version : 50617
 File Encoding         : utf-8

 Date: 03/22/2015 21:30:24 PM
*/

DROP DATABASE IF EXISTS 'noticesystem';
CREATE DATABASE 'noticesystem';
USE 'noticesystem';

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `admin`
-- ----------------------------
BEGIN;
INSERT INTO `admin` VALUES ('1', 'root', 'root');
COMMIT;

-- ----------------------------
--  Table structure for `iemail`
-- ----------------------------
DROP TABLE IF EXISTS `iemail`;
CREATE TABLE `iemail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  `username` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=eucjpms;

-- ----------------------------
--  Records of `iemail`
-- ----------------------------
BEGIN;
INSERT INTO `iemail` VALUES ('1', 'zhangyutong@zhangyutong.net', 'root', '1');
COMMIT;

-- ----------------------------
--  Table structure for `notice`
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `caption` text NOT NULL,
  `summary` text NOT NULL,
  `detail` text NOT NULL,
  `towho` text NOT NULL,
  `readed` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `notice`
-- ----------------------------
BEGIN;
INSERT INTO `notice` VALUES ('1', 'root', 'a', 'a', '<p>\n	fasdfasdfasdfa\n</p>\n<h1>\n	asdfasdfasdfasdfasdfsd\n</h1>\n<p>\n	', '{\"user\":[{\"name\":\"zhangyutong926\"},{\"name\":\"root\"}]}', '{“who_readed”:[{“name”:”root”}]}');
COMMIT;

-- ----------------------------
--  Table structure for `pemail`
-- ----------------------------
DROP TABLE IF EXISTS `pemail`;
CREATE TABLE `pemail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=eucjpms;

-- ----------------------------
--  Table structure for `public`
-- ----------------------------
DROP TABLE IF EXISTS `public`;
CREATE TABLE `public` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `caption` text NOT NULL,
  `summary` text NOT NULL,
  `detail` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('5', 'root', 'root'), ('6', 'zhangyutong926', '123456');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
