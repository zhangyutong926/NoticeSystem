<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>内部公告查看系统-登录|HSACNU-STUDY-PROJECT-X公告网</title>
<%
	String username=null;
	Cookie[] cookie=request.getCookies();
	if(cookie!=null){
		for(int i=0;i<cookie.length;i++){
			if(cookie[i].getName().equals("NSUN")==true){
				username=cookie[i].getValue();
			}
		}
	}
	if(!((username=="")||(username==null))){
		response.sendRedirect("./inner/index.jsp");
		return ;
	}
%>
<script type="text/javascript">
var username_verify=false;
var password_verify=false;

function updata_state(){
	var info=document.getElementById("info");
	if(username_verify==true && password_verify==true){
		info.style.display="none";
	}else{
		info.innerHTML="请将信息填写完整！";
		info.style.display="inline";
	}
}

function check_everything(){
	var username=document.getElementById("username");
	var password=document.getElementById("password");
	var check_username_area=document.getElementById("check_username_area");
	var check_password_area=document.getElementById("check_password_area");
	if(username.value!=""){
		check_username_area.setAttribute("src", "./images/ok.png");
		username_verify=true;
	}else{
		check_username_area.setAttribute("src", "./images/no.png");
		username_verify=false;
	}
	if(password.value!=""){
		check_password_area.setAttribute("src", "./images/ok.png");
		password_verify=true;
	}else{
		check_password_area.setAttribute("src", "./images/no.png");
		password_verify=false;
	}
	updata_state();
}

function submit_form() {
	check_everything();
	updata_state();
	var info=document.getElementById("info");
	if(username_verify==true && password_verify==true){
		var ajax=new XMLHttpRequest();
		ajax.onreadystatechange=function(){
			if(ajax.readyState==4 && ajax.status==200){
				var str=ajax.responseText;
				if(str=="true"){
					var username=document.getElementById("username").value;
					window.location.href="./SetCookie?username="+username;
				}else if(str=="false"){
					var info=document.getElementById("info");
					info.innerHTML="用户名或密码错误！";
					info.style.display="inline";
				}
			}
		};
		var username=document.getElementById("username");
		var password=document.getElementById("password");
		ajax.open("post", "./UserLogin", true);
		ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		ajax.send("username="+username.value+"&password="+password.value);
	}else{
		alert("请将登陆信息填写完整");
	}
}
</script>
</head>
<body>
<center>
<h1><font color="red">HSACNU-STUDY-PROJECT-X公告网 内部公告</font></h1>
欢迎访问首都师范大学附属中学IT技术社团公告和通知内部网站！
请您登陆后再查看内部公告！
<table border="1">
	<tr>
		<td>用户名：</td>
		<td><input type="text" name="username" id="username" style="width: 400px;" onblur="check_everything()"/><img id="check_username_area" /></td>
	</tr>
	<tr>
		<td>密码：</td>
		<td><input type="password" name="password" id="password" style="width: 400px;" onblur="check_everything()" /><img id="check_password_area" /></td>
	</tr>
	<tr>
		<td><input type="button" value="登录" onclick="submit_form()" /></td>
		<td><input type="reset" value="重填" />&nbsp;&nbsp;&nbsp;&nbsp;<font color="red" id="info" style="display: none;"></font></td>
	</tr>
</table>
</center>
</body>
</html>