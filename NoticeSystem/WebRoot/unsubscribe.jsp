<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
if(confirm("确定退订吗？")){
	var ajax=new XMLHttpRequest();
	ajax.onreadystatechange=function(){
		if(ajax.readyState==4 && ajax.status==200){
			if(ajax.responseText=="true"){
				alert("你将不再会收到推送信息！");
				window.close();
			}else{
				alert("内部错误，请通知金鸡独立以便我们可以解决问题：\n"+ajax.responseText);
				window.close();
			}
		}
	};
	ajax.open("post","./UnsubscribePublic",true);
	ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	ajax.send("id="+<%=request.getParameter("id")%>);
}else{
	alert("你还可以继续收到推送信息！");
	window.close();
}
</script>
<title>退订公众公告邮件推送|HSACNU-STUDY-PROJECT-X公告网</title>
</head>
<body>
</body>
</html>