<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%!
	//数据库内容
	class noticeData{
		int id;
		String username;
		String caption;
		String summary;
		String detail;
	}
%>
<title>公众首页|HSACNU-STUDY-PROJECT-X公告网</title>
<%
	noticeData[] nd=null;
	String sqls1="SELECT * FROM public";
	Class.forName("com.mysql.jdbc.Driver");
	Connection con=DriverManager.getConnection(application.getAttribute("dburl").toString()+"", application.getAttribute("username").toString(), application.getAttribute("password").toString());
	Statement sm=con.createStatement();
	ResultSet rs=sm.executeQuery(sqls1);
	rs.last();
	int len=rs.getRow();
	rs.beforeFirst();
	if(len!=0){
		nd=new noticeData[len];
		for(int i=0;i<len;++i){
			rs.next();
			nd[i]=new noticeData();
			nd[i].id=rs.getInt(1);
			nd[i].username=rs.getString(2);
			nd[i].caption=rs.getString(3);
			nd[i].summary=rs.getString(4);
			nd[i].detail=rs.getString(5);
		}
	}
%>
<script type="text/javascript">
function open_view(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var re_tr=document.getElementById("re-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	var new_child=document.createElement("tr");
	var new_td=document.createElement("td");
	new_child.setAttribute("id","trv-"+tr.id);
	var ajax=new XMLHttpRequest();
	var rehtml;
	ajax.onreadystatechange=function(){
		if(ajax.readyState==4 && ajax.status==200){
			rehtml=ajax.responseText;
			if(rehtml=="[];',.error"){
				alert("数据库错误，没有查到相关记录！");
			}else{
				new_td.innerHTML=rehtml;
				new_td.setAttribute("colspan","5");
				new_child.appendChild(new_td);
				table.insertBefore(new_child,re_tr);
				bu.setAttribute("onclick","shut_view(this.id)");
				bu.innerHTML="收起查看";
			}
		}
	};
	ajax.open("post","./Open",true);
	ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	ajax.send("id="+tr.id);
}

function shut_view(id){
	var this_ele=document.getElementById(id);
	var tr=this_ele.parentNode.parentNode;
	var parent=this_ele.parentNode.parentNode.parentNode;
	var old=document.getElementById("trv-"+tr.id);
	parent.removeChild(old);
	this_ele.setAttribute("onclick","open_view(this.id)");
	this_ele.innerHTML="展开查看";
}

function commit_email(){
	var ajax=new XMLHttpRequest();
	var email=document.getElementById("email").value;
	ajax.onreadystatechange=function(){
		if(ajax.readyState==4 && ajax.status==200){
			if(ajax.responseText=="true"){
				alert("提交成功，你将收到我们的通知推送！");
			}else{
				alert("提交失败，请将以下信息提交给金鸡独立，以便我们完善系统：\n"+ajax.responseText);
			}
		}
	}
	ajax.open("post", "./PublicEmail", true);
	ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	ajax.send("email="+email);
}
</script>
</head>
<body>
<center>
<h1><font color="red">公众首页|HSACNU-STUDY-PROJECT-X公告网</font></h1>
欢迎访问首都师范大学附属中学IT技术社团公告和通知官方网站！
<br/>
<table border="1">
	<tr>
		<td colspan="2">你是否需要我们向你的邮箱推送最新的公告？<br/>如果是，在下方填入你的邮箱：</td>
	</tr>
	<tr>
		<td><center><input type="text" id="email" name="email" style="width: 250px" /></center></td>
		<td><button onclick="commit_email()">提交</button></td>
	</tr>
</table>
以下是对外公告：
<table border="1" id="data_table">
	<tr>
		<th>序号</th>
		<th>标题</th>
		<th>摘要</th>
		<th>发布者</th>
		<th>详细信息</th>
	</tr>
<%
	if(len==0){
		out.println("	<tr>");
		out.println("		<td colspan=\"5\"><font color=\"red\">没有任何公告</font></td>");
		out.println("	</tr>");
	}else{
		for(int i=0;i<len;++i){
			out.println("	<tr id=\"tr"+nd[i].id+"\">");
			out.println("		<td>"+nd[i].id+"</td>");
			out.println("		<td>"+nd[i].caption+"</td>");
			out.println("		<td>"+nd[i].summary+"</td>");
			out.println("		<td>"+nd[i].username+"</td>");
			out.println("		<td><button id=\"button"+nd[i].id+"\" onclick=\"open_view(this.id)\">展开查看</button></td>");
			out.println("	</tr>");
			out.println("	<tr id=\"re-tr"+nd[i].id+"\" style=\"display: none; \"></tr>");
		}
	}
%>
</table>
如果你要查看对外公告，请访问<a href="./index.jsp">外部公告网</a>。
如果你要查看内部公告，请访问<a href="./inner-notice.jsp">内部公告网</a>。<br/>
如果你是管理员，请访问<a href="./admin-login.jsp">管理员登陆</a>。<br/>
如果你要查看本应用的开发者相关信息，请访问<a href="./about.html">开发者信息</a>。
</center>
</body>
</html>