<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%!
	//数据库内容
	class noticeData{
		int id;
		String username;
		String caption;
		String summary;
		String detail;
	}
%>
<title></title>
<%
	String username=null;
	Cookie[] cookie=request.getCookies();
	if(cookie!=null){
		for(int i=0;i<cookie.length;i++){
			if(cookie[i].getName().equals("NSAN")==true){
				username=cookie[i].getValue();
			}
		}
	}
	if((username=="")||(username==null)){
		response.sendRedirect("../admin-login.jsp");
		return ;
	}
	noticeData nd=new noticeData();
	int id=0;
	if(request.getParameter("id")!=null){
		id=Integer.parseInt(request.getParameter("id").substring(3));
		String sqls1="SELECT * FROM public WHERE id="+id;
		Class.forName("com.mysql.jdbc.Driver");
		Connection con=DriverManager.getConnection(application.getAttribute("dburl").toString()+"", application.getAttribute("username").toString(), application.getAttribute("password").toString());
		Statement sm=con.createStatement();
		ResultSet rs=sm.executeQuery(sqls1);
		rs.last();
		int len=rs.getRow();
		rs.beforeFirst();
		String info="";
		if(len!=0){
			for(int i=0;i<len;++i){
				rs.next();
				nd=new noticeData();
				nd.id=rs.getInt(1);
				nd.username=rs.getString(2);
				nd.caption=rs.getString(3);
				nd.summary=rs.getString(4);
				nd.detail=rs.getString(5);
			}
		}else{
			System.err.println("NoticeSystem-log：edit.jsp查询错误");
			info="内部错误：没有查到相应信息！";
			out.println(info);
			return ;
		}
	}else{
		id=-1;
		nd.id=-1;
		nd.caption="";
		nd.summary="";
		nd.detail="";
		nd.username=username;
	}
	
%>
<script>
	var id=<%=id%>;
	var username="<%=nd.username%>";
</script>
<script charset="utf-8" src="../editor/kindeditor.js"></script>
<script charset="utf-8" src="../editor/lang/zh_CN.js"></script>
<script src="./public-edit.js"></script>
</head>
<body>
<center>
<table border="1">
	<tr>
		<td>序号：</td>
		<td>
<%
	if(id!=-1){
		out.println(nd.id);
	}else{
		out.println("待分配");
	}
%>
		</td>
	</tr>
	<tr>
		<td>标题：</td>
		<td><input type="text" id="caption" name="caption" style="width: 650px;" value="<%=nd.caption %>" /></td>
	</tr>
	<tr>
		<td>简述：</td>
		<td><textarea id="summary" name="summary" style="width: 650px;height: 100px"><%=nd.summary %></textarea></td>
	</tr>
	<tr>
		<td>发布者：</td>
		<td><%=nd.username %></td>
	</tr>
	<tr>
		<td>详细信息：</td>
		<td>
			<textarea id="detail_editor" name="detail_editor" style="width:650px;height:300px;">
<%
	if(id!=-1){
		out.println(nd.detail);
	}else{
		
	}
%>
			</textarea>
		</td>
	</tr>
	<tr>
		<td><button id="commit-button" onclick="commit()">提交</button></td>
		<td>提交后请点击上方关闭编辑来关闭，如果不提交，直接点击关闭编辑。</td>
	</tr>
</table>
</center>
</body>
</html>