<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%!
	//数据库内容
	class userData{
		int id;
		String username;
		String password;
	}
%>
<title></title>
<%
	String username=null;
	Cookie[] cookie=request.getCookies();
	if(cookie!=null){
		for(int i=0;i<cookie.length;i++){
			if(cookie[i].getName().equals("NSAN")==true){
				username=cookie[i].getValue();
			}
		}
	}
	if((username=="")||(username==null)){
		response.sendRedirect("../admin-login.jsp");
		return ;
	}
	userData ud=null;
	int id=0;
	ud=new userData();
	if(request.getParameter("id")!=null){
		id=Integer.parseInt(request.getParameter("id").substring(3));
		String sqls1="SELECT * FROM user WHERE id="+id;
		Class.forName("com.mysql.jdbc.Driver");
		Connection con=DriverManager.getConnection(application.getAttribute("dburl").toString()+"", application.getAttribute("username").toString(), application.getAttribute("password").toString());
		Statement sm=con.createStatement();
		ResultSet rs=sm.executeQuery(sqls1);
		rs.last();
		int len=rs.getRow();
		rs.beforeFirst();
		String info="";
		if(len!=0){
			rs.next();
			ud.id=rs.getInt(1);
			ud.username=rs.getString(2);
			ud.password=rs.getString(3);
		}else{
			System.err.println("NoticeSystem-log：edit.jsp查询错误");
			info="内部错误：没有查到相应信息！";
			out.println(info);
			return ;
		}
	}else{
		id=-1;
		ud.id=-1;
		ud.username="";
		ud.password="";
	}
	
%>
<script>
	var id=<%=id%>;
</script>
<script src="./user-edit.js"></script>
</head>
<body>
<center>
<table border="1">
	<tr>
		<td>序号：</td>
		<td>
<%
	if(id!=-1){
		out.println(ud.id);
	}else{
		out.println("待分配");
	}
%>
		</td>
	</tr>
	<tr>
		<td>用户名：</td>
		<td><input type="text" id="username" name="username" style="width: 650px;" value="<%=ud.username %>" /></td>
	</tr>
	<tr>
		<td>密码：</td>
		<td><input type="text" id="password" name="password" style="width: 650px;" value="<%=ud.password %>"></td>
	</tr>
	<tr>
		<td><button id="commit-button" onclick="commit()">提交</button></td>
		<td>提交后请点击上方关闭编辑来关闭，如果不提交，直接点击关闭编辑。</td>
	</tr>
</table>
</center>
</body>
</html>