<%@page import="net.sf.json.JSONArray"%>
<%@page import="net.sf.json.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%!
	class userData{
		int id;
		String name;
		boolean isChecked=false;
	}
%>
<%
	String username=null;
	Cookie[] cookie=request.getCookies();
	if(cookie!=null){
		for(int i=0;i<cookie.length;i++){
			if(cookie[i].getName().equals("NSAN")==true){
				username=cookie[i].getValue();
			}
		}
	}
	int id=Integer.parseInt(request.getParameter("id"));
	if(id==-1){
		out.print("这是新添加的条目，由于安全问题，不能允许编辑用户访问权限，请添加后刷新页面并使用编辑条目进行编辑。添加后的条目没有设置访问权限时，没有用户可以访问！");
		return ;
	}
	String sqls1="SELECT id,name FROM USER";
	Class.forName("com.mysql.jdbc.Driver");
	Connection con=DriverManager.getConnection(application.getAttribute("dburl").toString()+"", application.getAttribute("username").toString(), application.getAttribute("password").toString());
	Statement sm=con.createStatement();
	ResultSet rs=sm.executeQuery(sqls1);
	rs.last();
	int user_len=rs.getRow();
	rs.beforeFirst();
	userData[] ud=new userData[user_len];
	for(int i=0;i<user_len;++i){
		rs.next();
		ud[i]=new userData();
		ud[i].id=rs.getInt(1);
		ud[i].name=rs.getString(2);
	}
	String sqls2="SELECT towho FROM notice WHERE id="+id;
	rs=sm.executeQuery(sqls2);
	rs.next();
	String towho=rs.getString(1);
	if(towho!=null && towho!=""){
		JSONObject jo=JSONObject.fromObject(towho);
		JSONArray ja=jo.getJSONArray("user");
		for(int i=0;i<ja.size();++i){
			JSONObject jot=ja.getJSONObject(i);
			String tname=jot.getString("name");
			for(int j=0;j<user_len;++j){
				if(ud[j].name.equals(tname)==true){
					ud[j].isChecked=true;
				}
			}
		}
	}
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<script type="text/javascript">
<%
	out.println("var users=new Array(");
	for(int i=0;i<user_len;++i){
		if(i==user_len-1){
			out.println("\"user-"+ud[i].id+"\"");
		}else{
			out.println("\"user-"+ud[i].id+"\",");
		}
	}
	out.println(");");
	out.println("var usernames=new Array(");
	for(int i=0;i<user_len;++i){
		if(i==user_len-1){
			out.println("\"username-"+ud[i].id+"\"");
		}else{
			out.println("\"username-"+ud[i].id+"\",");
		}
	}
	out.println(");");
	out.println("var id="+id);
%>
function save(){
	var json_result="{\"user\":[";
	for(var i=0;i<users.length;++i){
		if(document.getElementById(users[i]).checked==true){
			if(i==users.length-1){
				var temp="{\"name\":\""+document.getElementById(usernames[i]).innerHTML+"\"}";
				json_result+=temp;
			}else{
				var temp="{\"name\":\""+document.getElementById(usernames[i]).innerHTML+"\"},";
				json_result+=temp;
			}
		}
	}
	json_result+="]}";
	var ajax=new XMLHttpRequest();
	ajax.onreadystatechange=function(){
		if(ajax.readyState==4 && ajax.status==200){
			if(ajax.responseText=="succes"){
				alert("访问权限信息成功保存！");
			}else{
				alert("错误，代码是："+ajax.responseText);
			}
		}
	};
	ajax.open("POST", "./ChooseUser", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("users="+json_result+"&id="+id);
}
</script>
</head>
<body>
请注意：用户访问权限必须按以下按钮保存，否则由于网络问题将无法进行权限更新！<br/>
<button onclick="save()">保存权限</button><br/>
<%
	for(int i=0;i<user_len;++i){
		out.println("<input type=\"checkbox\" id=\"user-"+ud[i].id+"\" name=\"user-"+ud[i].id+"\""+(ud[i].isChecked==true?" checked=\"checked\"":"")+" />");
		out.println(ud[i].id+":"+"<a id=\"username-"+ud[i].id+"\">"+ud[i].name+"</a>");
		out.println("<br/>");
	}
%>
</body>
</html>