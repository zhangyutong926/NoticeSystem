function commit() {
	var username=document.getElementById("username").value;
	var password=document.getElementById("password").value;
	if(id!=-1){
		if(confirm("确认提交吗？")==true){
			var ajax=new XMLHttpRequest();
			ajax.onreadystatechange=function(){
				if(ajax.readyState==4 && ajax.status==200){
					var result=ajax.responseText;
					if(result="true"){
						alert("提交成功，数据已经写入数据库，请您点击收起编辑后再点击刷新当前页面以便查看效果！");
					}else{
						alert("提交失败，详细信息：\n"+result);
					}
				}
			};
			ajax.open("post", "./UserEdit", true);
			ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			ajax.send("id="+id+"&username="+username+"&password="+password);
		}else{
			alert("您编辑的信息尚未提交！");
		}
	}else{
		if(confirm("确认提交吗？")==true){
			var ajax=new XMLHttpRequest();
			ajax.onreadystatechange=function(){
				if(ajax.readyState==4 && ajax.status==200){
					var result=ajax.responseText;
					if(result="true"){
						alert("提交成功，数据已经写入数据库，请您点击收起编辑后再点击刷新当前页面以便查看效果！");
					}else{
						alert("提交失败，详细信息：\n"+result);
					}
				}
			};
			ajax.open("post", "./AddUser", true);
			ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			ajax.send("username="+username+"&password="+password);
		}else{
			alert("您编辑的信息尚未提交！");
		}
	}
}
