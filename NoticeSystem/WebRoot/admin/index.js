function public_open_view(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var re_tr=document.getElementById("rep-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	var new_child=document.createElement("tr");
	var new_td=document.createElement("td");
	new_child.setAttribute("id","trvp-"+tr.id);
	var ajax=new XMLHttpRequest();
	var rehtml;
	ajax.onreadystatechange=function(){
		if(ajax.readyState==4 && ajax.status==200){
			rehtml=ajax.responseText;
			if(rehtml=="[];',.error"){
				alert("数据库错误，没有查到相关记录！");
			}else{
				new_td.innerHTML=rehtml;
				new_td.setAttribute("colspan","5");
				new_child.appendChild(new_td);
				table.insertBefore(new_child,re_tr);
				bu.setAttribute("onclick","public_shut_view(this.id)");
				bu.innerHTML="收起查看";
			}
		}
	};
	ajax.open("post","./PublicOpen",true);
	ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	ajax.send("id="+tr.id);
}

function public_shut_view(id){
	var this_ele=document.getElementById(id);
	var tr=this_ele.parentNode.parentNode;
	var parent=this_ele.parentNode.parentNode.parentNode;
	var old=document.getElementById("trvp-"+tr.id);
	parent.removeChild(old);
	this_ele.setAttribute("onclick","public_open_view(this.id)");
	this_ele.innerHTML="展开查看";
}

function public_open_edit(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var re_tr=document.getElementById("rep-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	var new_child=document.createElement("tr");
	new_child.setAttribute("id","trep-"+tr.id);
	var new_td=document.createElement("td");
	new_td.setAttribute("colspan","5");
	var new_iframe=document.createElement("iframe");
	new_iframe.setAttribute("src","./public-edit.jsp?id="+tr.id+"&"+Math.random());
	new_iframe.style.width="750px";
	new_iframe.style.height="500px";
	new_td.appendChild(new_iframe);
	new_child.appendChild(new_td);
	table.insertBefore(new_child,re_tr);
	bu.innerHTML="收起编辑";
	bu.setAttribute("onclick","public_shut_edit(this.id)");
}

function public_shut_edit(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var old_tr=document.getElementById("trep-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	table.removeChild(old_tr);
	bu.innerHTML="展开编辑";
	bu.setAttribute("onclick","public_open_edit(this.id)");
}

function flush(){
	location.reload();
}

function public_remo(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var re_tr=document.getElementById("rep-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	if(confirm("确定删除？")==true){
		var ajax=new XMLHttpRequest();
		ajax.onreadystatechange=function(){
			if(ajax.readyState==4 && ajax.status==200){
				var rehtml=ajax.responseText;
				if(rehtml=="true"){
					alert("已经将修改提交到数据库，请刷新本页面来查看效果！");
				}else{
					alert("出现错误，详细信息:\n"+rehtml);
				}
			}
		}
		ajax.open("post","./RemoPublic",true);
		ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		ajax.send("id="+tr.id);
	}else{
		alert("你的更改没有被提交！");
	}
}

function inner_send(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var re_tr=document.getElementById("rep-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	if(confirm("确定发送？这将会给所有有权限访问这条公告的用户发送这条公告（如果这个用户提供了自己的邮箱）。")==true){
		var ajax=new XMLHttpRequest();
		ajax.onreadystatechange=function(){
			if(ajax.readyState==4 && ajax.status==200){
				var rehtml=ajax.responseText;
				if(rehtml=="succes"){
					alert("已经将发送请求提交到邮件核心，邮件可能会延迟一段时间发送，如果已经有邮件请求正在被处理的话！");
				}else{
					alert("出现错误，详细信息:\n"+rehtml);
				}
			}
		}
		ajax.open("post","./InnerSend",true);
		ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		ajax.send("id="+tr.id);
	}else{
		alert("你的操作没有被提交！");
	}
}

function inner_remo(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var re_tr=document.getElementById("rep-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	if(confirm("确定删除？")==true){
		var ajax=new XMLHttpRequest();
		ajax.onreadystatechange=function(){
			if(ajax.readyState==4 && ajax.status==200){
				var rehtml=ajax.responseText;
				if(rehtml=="true"){
					alert("已经将修改提交到数据库，请刷新本页面来查看效果！");
				}else{
					alert("出现错误，详细信息:\n"+rehtml);
				}
			}
		}
		ajax.open("post","./RemoInner",true);
		ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		ajax.send("id="+tr.id);
	}else{
		alert("你的更改没有被提交！");
	}
}

function public_add_new(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var table=bu.parentNode.parentNode.parentNode;
	var new_child=document.createElement("tr");
	new_child.setAttribute("id","trnp1");
	var new_td=document.createElement("td");
	new_td.setAttribute("colspan","5");
	var new_iframe=document.createElement("iframe");
	new_iframe.setAttribute("src","./public-edit.jsp"+"&"+Math.random());
	new_iframe.style.width="750px";
	new_iframe.style.height="500px";
	new_td.appendChild(new_iframe);
	new_child.appendChild(new_td);
	table.appendChild(new_child);
	bu.setAttribute("onclick","public_shut_new(this.id)");
	bu.innerHTML="取消添加新公开公告";
}

function public_shut_new(id){
	var bu=document.getElementById(id);
	var table=bu.parentNode.parentNode.parentNode;
	var tr=document.getElementById("trnp1");
	table.removeChild(tr);
	bu.setAttribute("onclick","public_add_new(this.id)");
	bu.innerHTML="添加新公开公告";
}

function inner_add_new(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var table=bu.parentNode.parentNode.parentNode;
	var new_child=document.createElement("tr");
	new_child.setAttribute("id","trni1");
	var new_td=document.createElement("td");
	new_td.setAttribute("colspan","5");
	var new_iframe=document.createElement("iframe");
	new_iframe.setAttribute("src","./inner-edit.jsp"+"&"+Math.random());
	new_iframe.style.width="750px";
	new_iframe.style.height="500px";
	new_td.appendChild(new_iframe);
	new_child.appendChild(new_td);
	table.appendChild(new_child);
	bu.setAttribute("onclick","inner_shut_new(this.id)");
	bu.innerHTML="取消添加新内部公告";
}

function inner_shut_new(id){
	var bu=document.getElementById(id);
	var table=bu.parentNode.parentNode.parentNode;
	var tr=document.getElementById("trni1");
	table.removeChild(tr);
	bu.setAttribute("onclick","inner_add_new(this.id)");
	bu.innerHTML="添加新内部公告";
}

function inner_open_view(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var re_tr=document.getElementById("rei-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	var new_child=document.createElement("tr");
	var new_td=document.createElement("td");
	new_child.setAttribute("id","trvi-"+tr.id);
	var ajax=new XMLHttpRequest();
	var rehtml;
	ajax.onreadystatechange=function(){
		if(ajax.readyState==4 && ajax.status==200){
			rehtml=ajax.responseText;
			if(rehtml=="[];',.error"){
				
				alert("数据库错误，没有查到相关记录！");
			}else{
				new_td.innerHTML=rehtml;
				new_td.setAttribute("colspan","5");
				new_child.appendChild(new_td);
				table.insertBefore(new_child,re_tr);
				bu.setAttribute("onclick","inner_shut_view(this.id)");
				bu.innerHTML="收起查看";
			}
		}
	};
	ajax.open("post","./InnerOpen",true);
	ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	ajax.send("id="+tr.id);
}

function inner_shut_view(id){
	var this_ele=document.getElementById(id);
	var tr=this_ele.parentNode.parentNode;
	var parent=this_ele.parentNode.parentNode.parentNode;
	var old=document.getElementById("trvi-"+tr.id);
	parent.removeChild(old);
	this_ele.setAttribute("onclick","inner_open_view(this.id)");
	this_ele.innerHTML="展开查看";
}

function inner_open_edit(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var re_tr=document.getElementById("rei-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	var new_child=document.createElement("tr");
	new_child.setAttribute("id","trei-"+tr.id);
	var new_td=document.createElement("td");
	new_td.setAttribute("colspan","5");
	var new_iframe=document.createElement("iframe");
	new_iframe.setAttribute("src","./inner-edit.jsp?id="+tr.id+"&"+Math.random());
	new_iframe.style.width="750px";
	new_iframe.style.height="500px";
	new_td.appendChild(new_iframe);
	new_child.appendChild(new_td);
	table.insertBefore(new_child,re_tr);
	bu.innerHTML="收起编辑";
	bu.setAttribute("onclick","inner_shut_edit(this.id)");
}

function inner_shut_edit(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var old_tr=document.getElementById("trei-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	table.removeChild(old_tr);
	bu.innerHTML="展开编辑";
	bu.setAttribute("onclick","inner_open_edit(this.id)");
}

function user_add_new(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var table=bu.parentNode.parentNode.parentNode;
	var new_child=document.createElement("tr");
	new_child.setAttribute("id","trnu1");
	var new_td=document.createElement("td");
	new_td.setAttribute("colspan","4");
	var new_iframe=document.createElement("iframe");
	new_iframe.setAttribute("src","./user-edit.jsp"+"&"+Math.random());
	new_iframe.style.width="750px";
	new_iframe.style.height="500px";
	new_td.appendChild(new_iframe);
	new_child.appendChild(new_td);
	table.appendChild(new_child);
	bu.setAttribute("onclick","user_shut_new(this.id)");
	bu.innerHTML="取消添加新内部查看用户";
}

function user_shut_new(id){
	var this_ele=document.getElementById(id);
	var tr=this_ele.parentNode.parentNode;
	var parent=this_ele.parentNode.parentNode.parentNode;
	var old=document.getElementById("trnu1");
	parent.removeChild(old);
	this_ele.setAttribute("onclick","user_add_new(this.id)");
	this_ele.innerHTML="添加新内部查看用户";
}

function user_open_edit(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var re_tr=document.getElementById("reu-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	var new_child=document.createElement("tr");
	new_child.setAttribute("id","treu-"+tr.id);
	var new_td=document.createElement("td");
	new_td.setAttribute("colspan","4");
	var new_iframe=document.createElement("iframe");
	new_iframe.setAttribute("src","./user-edit.jsp?id="+tr.id+"&"+Math.random());
	new_iframe.style.width="750px";
	new_iframe.style.height="500px";
	new_td.appendChild(new_iframe);
	new_child.appendChild(new_td);
	table.insertBefore(new_child,re_tr);
	bu.innerHTML="收起编辑";
	bu.setAttribute("onclick","user_shut_edit(this.id)");
}

function user_shut_edit(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var old_tr=document.getElementById("treu-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	table.removeChild(old_tr);
	bu.innerHTML="展开编辑";
	bu.setAttribute("onclick","user_open_edit(this.id)");
}

function user_remo(id){
	var bu=document.getElementById(id);
	var tr=bu.parentNode.parentNode;
	var re_tr=document.getElementById("rep-"+tr.id);
	var table=bu.parentNode.parentNode.parentNode;
	if(confirm("确定删除？")==true){
		var ajax=new XMLHttpRequest();
		ajax.onreadystatechange=function(){
			if(ajax.readyState==4 && ajax.status==200){
				var rehtml=ajax.responseText;
				if(rehtml=="true"){
					alert("已经将修改提交到数据库，请刷新本页面来查看效果！");
				}else{
					alert("出现错误，详细信息:\n"+rehtml);
				}
			}
		}
		ajax.open("post","./RemoUser",true);
		ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		ajax.send("id="+tr.id);
	}else{
		alert("你的更改没有被提交！");
	}
}
