<%@page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%!
	//数据库内容
	class noticeData{
		int id;
		String username;
		String caption;
		String summary;
		String detail;
	}
	
	class userData{
		int id;
		String username;
		String password;
	}
%>
<title>管理后台|HSACNU-STUDY-PROJECT-X公告网</title>
<%
	String username=null;
	Cookie[] cookie=request.getCookies();
	if(cookie!=null){
		for(int i=0;i<cookie.length;i++){
			if(cookie[i].getName().equals("NSAN")==true){
				username=cookie[i].getValue();
			}
		}
	}
	if((username=="")||(username==null)){
		response.sendRedirect("../admin-login.jsp");
		return ;
	}
	noticeData[] ndp=null;
	noticeData[] ndi=null;
	String sqls1="SELECT * FROM public";
	Class.forName("com.mysql.jdbc.Driver");
	Connection con=DriverManager.getConnection(application.getAttribute("dburl").toString()+"", application.getAttribute("username").toString(), application.getAttribute("password").toString());
	Statement sm=con.createStatement();
	ResultSet rs=sm.executeQuery(sqls1);
	rs.last();
	int len=rs.getRow();
	rs.beforeFirst();
	if(len!=0){
		ndp=new noticeData[len];
		for(int i=0;i<len;++i){
			rs.next();
			ndp[i]=new noticeData();
			ndp[i].id=rs.getInt(1);
			ndp[i].username=rs.getString(2);
			ndp[i].caption=rs.getString(3);
			ndp[i].summary=rs.getString(4);
			ndp[i].detail=rs.getString(5);
		}
	}
	String sqls2="SELECT * FROM notice";
	rs=sm.executeQuery(sqls2);
	rs.last();
	int len2=rs.getRow();
	rs.beforeFirst();
	if(len2!=0){
		ndi=new noticeData[len2];
		for(int i=0;i<len2;++i){
			rs.next();
			ndi[i]=new noticeData();
			ndi[i].id=rs.getInt(1);
			ndi[i].username=rs.getString(2);
			ndi[i].caption=rs.getString(3);
			ndi[i].summary=rs.getString(4);
			ndi[i].detail=rs.getString(5);
		}
	}
	userData[] ud=null;
	String sqls3="SELECT * FROM user";
	rs=sm.executeQuery(sqls3);
	rs.last();
	int len3=rs.getRow();
	rs.beforeFirst();
	if(len3!=0){
		ud=new userData[len3];
		for(int i=0;i<len3;++i){
			rs.next();
			ud[i]=new userData();
			ud[i].id=rs.getInt(1);
			ud[i].username=rs.getString(2);
			ud[i].password=rs.getString(3);
		}
	}
%>
<script type="text/javascript" src="./index.js"></script>
</head>
<body>
<center>
<h1><font color="red">管理后台|HSACNU-STUDY-PROJECT-X公告网</font></h1>
你好，亲爱的<%=username %>，欢迎进入内部公告网！&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="./UserLogout">安全退出</a><br/>
<font color="red">特别注意：如果需要添加或删除或编辑拥有管理权限的用户，请与服务器管理员联系，并通过数据库添加！在本页面添加或删除或编辑的用户只有查看内部公告的权限！</font>
<br/>
如果你更改了数据，请<button onclick="flush()">刷新当前页面</button>后再查看，否则将无法看到更改！
<table style="vertical-align: top;">
<tr>
<th><center>外部公告：</center></th>
<th><center>内部公告：</center></th>
<th><center>内部查看用户管理：</center></th>
</tr>
<tr>
<td style="vertical-align: top;">
<table border="1" id="public_data_table">
	<tr>
		<th>序号</th>
		<th>标题</th>
		<th>摘要</th>
		<th>发布者</th>
		<th>详细信息</th>
	</tr>
<%
	if(len==0){
		out.println("	<tr>");
		out.println("		<td colspan=\"5\"><font color=\"red\">没有任何公开公告</font></td>");
		out.println("	</tr>");
	}else{
		for(int i=0;i<len;++i){
			out.println("	<tr id=\"trp"+ndp[i].id+"\">");
			out.println("		<td>"+ndp[i].id+"</td>");
			out.println("		<td>"+ndp[i].caption+"</td>");
			out.println("		<td>"+ndp[i].summary+"</td>");
			out.println("		<td>"+ndp[i].username+"</td>");
			out.println("		<td>");
			out.println("			<button id=\"public-view"+ndp[i].id+"\" onclick=\"public_open_view(this.id)\">展开查看</button>");
			out.println("			<button id=\"public-edit"+ndp[i].id+"\" onclick=\"public_open_edit(this.id)\">展开编辑</button>");
			out.println("			<button id=\"public-remo"+ndp[i].id+"\" onclick=\"public_remo(this.id)\">删除公告</button>");
			out.println("		</td>");
			out.println("	</tr>");
			out.println("	<tr id=\"rep-trp"+ndp[i].id+"\" style=\"display: none; \"></tr>");
		}
	}
%>
<tr>
	<td colspan="5"><button id="public_add_new" onclick="public_add_new(this.id)">添加新公开公告</button></td>
</tr>
</table>
</td>
<td style="vertical-align: top;">
<table border="1" id="inner_data_table">
	<tr>
		<th>序号</th>
		<th>标题</th>
		<th>摘要</th>
		<th>发布者</th>
		<th>详细信息</th>
	</tr>
<%
	if(len2==0){
		out.println("	<tr>");
		out.println("		<td colspan=\"5\"><font color=\"red\">没有任何内部公告</font></td>");
		out.println("	</tr>");
	}else{
		for(int i=0;i<len2;++i){
			out.println("	<tr id=\"tri"+ndi[i].id+"\">");
			out.println("		<td>"+ndi[i].id+"</td>");
			out.println("		<td>"+ndi[i].caption+"</td>");
			out.println("		<td>"+ndi[i].summary+"</td>");
			out.println("		<td>"+ndi[i].username+"</td>");
			out.println("		<td>");
			out.println("			<button id=\"inner-view"+ndi[i].id+"\" onclick=\"inner_open_view(this.id)\">展开查看</button>");
			out.println("			<button id=\"inner-edit"+ndi[i].id+"\" onclick=\"inner_open_edit(this.id)\">展开编辑</button>");
			out.println("			<button id=\"inner-remo"+ndi[i].id+"\" onclick=\"inner_remo(this.id)\">删除公告</button>");
			out.println("			<button id=\"inner-send"+ndi[i].id+"\" onclick=\"inner_send(this.id)\">发送邮件提醒</button>");
			out.println("		</td>");
			out.println("	</tr>");
			out.println("	<tr id=\"rei-tri"+ndi[i].id+"\" style=\"display: none; \"></tr>");
		}
	}
%>
<tr>
	<td colspan="5"><button id="inner_add_new" onclick="inner_add_new(this.id)">添加新公开公告</button></td>
</tr>
</table>
</td>
<td style="vertical-align: top;">
<table id="user_data_table" border="1">
	<tr>
		<th>序号</th>
		<th>用户名</th>
		<th>密码</th>
		<th>相关操作</th>
	</tr>
<%
	if(len3==0){
		out.println("	<tr>");
		out.println("		<td colspan=\"5\"><font color=\"red\">没有任何内部查看用户</font></td>");
		out.println("	</tr>");
	}else{
		for(int i=0;i<len3;++i){
			out.println("	<tr id=\"tru"+ud[i].id+"\">");
			out.println("		<td>"+ud[i].id+"</td>");
			out.println("		<td>"+ud[i].username+"</td>");
			out.println("		<td>"+ud[i].password+"</td>");
			out.println("		<td>");
			out.println("			<button id=\"user-edit"+ud[i].id+"\" onclick=\"user_open_edit(this.id)\">展开编辑</button>");
			out.println("			<button id=\"user-remo"+ud[i].id+"\" onclick=\"user_remo(this.id)\">删除用户</button>");
			out.println("		</td>");
			out.println("	</tr>");
			out.println("	<tr id=\"reu-tru"+ud[i].id+"\" style=\"display: none; \"></tr>");
		}
	}
%>
<tr>
	<td colspan="4"><button id="user_add_new" onclick="user_add_new(this.id)">添加新内部查看用户</button></td>
</tr>
</table>
</td>
</tr>
</table>
如果你要查看对外公告，请访问<a href="../index.jsp">外部公告网</a>。
如果你要查看内部公告，请访问<a href="../inner-notice.jsp">内部公告网</a>。<br/>
如果你是管理员，请访问<a href="../admin-login.jsp">管理员登陆</a>。<br/>
如果你要查看本应用的开发者相关信息，请访问<a href="../about.html">开发者信息</a>。
</center>
</body>
</html>